package com.example.gitPractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitPracticeApplication.class, args);

		System.out.println("1st commit");

		System.out.println("2nd commit");

		System.out.println("3rd commit");

		System.out.println("4th commit");

	}

}
